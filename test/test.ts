import { tap, expect } from 'tapbundle'
import * as qenv from 'qenv'
import * as smartmental from 'smartmental'

let testQenv: qenv.Qenv
tap.test('|| setup the test environment', async () => {
  testQenv = new qenv.Qenv('./', './.nogit/')
})

// -----------------------------------------
// Lets start with testing
// -----------------------------------------
import * as smartmonitor from '../ts/index'

let testSmartmonitor: smartmonitor.Smartmonitor

tap.test('should create a smartmonitor instance', async () => {
  testSmartmonitor = new smartmonitor.Smartmonitor()
  expect(testSmartmonitor).to.be.instanceof(smartmonitor.Smartmonitor)
})

tap.test('should create a smartmonitor instance', async () => {
  testSmartmonitor = new smartmonitor.Smartmonitor()
  expect(testSmartmonitor).to.be.instanceof(smartmonitor.Smartmonitor)
})

tap.test('should add smartmental instance', async () => {
  testSmartmonitor.addInstrumental({
    apiKey: process.env.INSTRUMENTAL_API_KEY
  })
  expect(testSmartmonitor.smartmentalInstance).to.be.instanceof(smartmental.Smartmental)
})

tap.test('should send notice', async () => {
  testSmartmonitor.notice('testing smartmonitor')
  testSmartmonitor.increment('smartmonitor.testRun', 1)
})

tap.test('should close the connection', async () => {
  testSmartmonitor.smartmentalInstance.close()
})

tap.start()
