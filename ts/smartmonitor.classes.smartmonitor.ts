import * as plugins from './smartmonitor.plugins'

import { Smartmental, ISmartmentalConstructorOptions } from 'smartmental'

export class Smartmonitor {
  smartmentalInstance: Smartmental

  addInstrumental (optionsArg: ISmartmentalConstructorOptions) {
    this.smartmentalInstance = new plugins.smartmental.Smartmental({
      apiKey: optionsArg.apiKey
    })
  }

  addSentry (optionsArg) {
    //
  }

  addSmartanalytics

  increment (metricName: string, value: number) {
    if (this.smartmentalInstance) {
      this.smartmentalInstance.increment(metricName, value)
    } else {
      plugins.beautylog.warn('no smartmonitor instance registered!!!')
    }
  }

  gauge (metricName: string, value: number) {
    if (this.smartmentalInstance) {
      this.smartmentalInstance.gauge(metricName, value)
    } else {
      plugins.beautylog.warn('no smartmonitor instance registered!!!')
    }
  }

  notice (messageArg: string) {
    if (this.smartmentalInstance) {
      this.smartmentalInstance.notice(messageArg)
    } else {
      plugins.beautylog.warn('no smartmonitor instance registered!!!')
    }
  }
}
