"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const plugins = require("./smartmonitor.plugins");
class Smartmonitor {
    addInstrumental(optionsArg) {
        this.smartmentalInstance = new plugins.smartmental.Smartmental({
            apiKey: optionsArg.apiKey
        });
    }
    addSentry(optionsArg) {
        //
    }
    increment(metricName, value) {
        if (this.smartmentalInstance) {
            this.smartmentalInstance.increment(metricName, value);
        }
        else {
            plugins.beautylog.warn('no smartmonitor instance registered!!!');
        }
    }
    gauge(metricName, value) {
        if (this.smartmentalInstance) {
            this.smartmentalInstance.gauge(metricName, value);
        }
        else {
            plugins.beautylog.warn('no smartmonitor instance registered!!!');
        }
    }
    notice(messageArg) {
        if (this.smartmentalInstance) {
            this.smartmentalInstance.notice(messageArg);
        }
        else {
            plugins.beautylog.warn('no smartmonitor instance registered!!!');
        }
    }
}
exports.Smartmonitor = Smartmonitor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRtb25pdG9yLmNsYXNzZXMuc21hcnRtb25pdG9yLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vdHMvc21hcnRtb25pdG9yLmNsYXNzZXMuc21hcnRtb25pdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsa0RBQWlEO0FBSWpEO0lBR0UsZUFBZSxDQUFFLFVBQTBDO1FBQ3pELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQzdELE1BQU0sRUFBRSxVQUFVLENBQUMsTUFBTTtTQUMxQixDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsU0FBUyxDQUFFLFVBQVU7UUFDbkIsRUFBRTtJQUNKLENBQUM7SUFJRCxTQUFTLENBQUUsVUFBa0IsRUFBRSxLQUFhO1FBQzFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUE7UUFDdkQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsd0NBQXdDLENBQUMsQ0FBQTtRQUNsRSxDQUFDO0lBQ0gsQ0FBQztJQUVELEtBQUssQ0FBRSxVQUFrQixFQUFFLEtBQWE7UUFDdEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUNuRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFBO1FBQ2xFLENBQUM7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFFLFVBQWtCO1FBQ3hCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFBO1FBQ2xFLENBQUM7SUFDSCxDQUFDO0NBQ0Y7QUF0Q0Qsb0NBc0NDIn0=