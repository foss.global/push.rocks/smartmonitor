import { Smartmental, ISmartmentalConstructorOptions } from 'smartmental';
export declare class Smartmonitor {
    smartmentalInstance: Smartmental;
    addInstrumental(optionsArg: ISmartmentalConstructorOptions): void;
    addSentry(optionsArg: any): void;
    addSmartanalytics: any;
    increment(metricName: string, value: number): void;
    gauge(metricName: string, value: number): void;
    notice(messageArg: string): void;
}
